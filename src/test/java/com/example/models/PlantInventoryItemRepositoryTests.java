package com.example.models;

import com.example.DemoApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Joonas Papoonas on 22/02/2017.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DemoApplication.class)
//@Sql(scripts="plants-dataset.sql")
@DirtiesContext(classMode=DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class PlantInventoryItemRepositoryTests {
    @Autowired
    PlantInventoryItemRepository plantInventoryItemRepo;

    /*
     * Check there is at least one item for a given entry available for rental in a given period of time (strict version).
     */
    @Test
    public void isItemAvailable() {
        assertThat(plantInventoryItemRepo.isItemAvailableForRental(1, LocalDate.of(2017, 2, 20), LocalDate.of(2017, 3, 20))).isEqualTo(true);
    }

    /*
     * Query the list of plants that have not been hired during the last 6 months.
     */
    @Test
    public void listNotHiredPlants() {
        assertThat(plantInventoryItemRepo.hasNotBeenHired().size()).isEqualTo(1);
    }
}
