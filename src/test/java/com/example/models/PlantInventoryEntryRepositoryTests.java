package com.example.models;

import com.example.DemoApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Joonas Papoonas on 22/02/2017.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DemoApplication.class)
@DirtiesContext(classMode=DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class PlantInventoryEntryRepositoryTests {
    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepo;

    /*
     * Query the list of available plants for rental in the plant catalog using the plant name (a substring of it) and the rental period.
     */
    @Test
    public void listAvailablePlants() {
        assertThat(plantInventoryEntryRepo.getAvailablePlantsForRental("Mini", LocalDate.of(2017, 2, 20), LocalDate.of(2017, 3, 20)).size()).isEqualTo(1);
    }
}