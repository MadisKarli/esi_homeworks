package com.example.models;

import com.example.DemoApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Joonas Papoonas on 22/02/2017.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DemoApplication.class)
@DirtiesContext(classMode=DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class MaintenanceTaskRepositoryTests {
    @Autowired
    MaintenanceTaskRepository maintenanceTaskRepo;
    /*
     * Compute the cost due to corrective repairs per year for the last 5 years (the query result must include the year and the cost associated with that year)
    */
    @Test
    public void listCorrectiveRepairsCost() {
        List<YearSum> correct = new ArrayList<>();
        YearSum cr = new YearSum(2017, BigDecimal.valueOf(300.00).setScale(2));
        correct.add(cr);
        assertThat(maintenanceTaskRepo.getCorrectiveRepairsPricePerYear()).isEqualTo(correct);
    }
}
