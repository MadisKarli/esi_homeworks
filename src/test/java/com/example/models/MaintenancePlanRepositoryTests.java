package com.example.models;

import com.example.DemoApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.Year;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Joonas Papoonas on 22/02/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = DemoApplication.class)
@DirtiesContext(classMode=DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class MaintenancePlanRepositoryTests {
    @Autowired
    MaintenancePlanRepository maintenancePlanRepo;

    /*
     * Compute the number of corrective repairs per year for the last 5 years (the query result must include the year and the count of repairs in that year).
     */
    @Test
    public void listCorrectiveRepairs() {
        List<YearCount> correct = new ArrayList<>();
        YearCount cr = new YearCount(2017, 1);
        correct.add(cr);
        assertThat(maintenancePlanRepo.getCorrectiveRepairsPerYear()).isEqualTo(correct);
}



}
