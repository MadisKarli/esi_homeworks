package com.example.models;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by gurudas on 21.02.17.
 */
@Entity
@Data
public class MaintenanceTask {
    @Id @GeneratedValue
    Long id;
    String description;

    @Enumerated(EnumType.STRING)
    TypeOfWork type_of_work;

    @Embedded
    BusinessPeriod rentalPeriod;

    BigDecimal price;

    @ManyToOne
    PlantReservation reservations;

}
