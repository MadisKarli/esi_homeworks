package com.example.models;

public enum EquipmentCondition {
    SERVICEABLE, UNSERVICEABLE_REPAIRABLE, UNSERVICEABLE_INCOMPLETE, UNSERVICEABLE_CONDEMNED
}
