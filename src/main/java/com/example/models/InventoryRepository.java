package com.example.models;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by zekrix on 22.02.17.
 */
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface InventoryRepository extends JpaRepository<PlantInventoryEntry, Long>, CustomInventoryRepository  {
  @Query(value = "select i.name\n" +
          "\tfrom PLANT_INVENTORY_ENTRY  i\n" +
          "\twhere LOWER(i.name) like ?1\n" +
          "\tand i.id not in \n" +
          "\t\t(select r.plant_id from PLANT_RESERVATION  r\n" +
          "\t\t\twhere not(?2 <= r.start_date) or\n" +
          "\t\t\t ?3 > r.end_date)", nativeQuery=true)
  List<PlantInventoryEntry> findAvailablePlants(String name, LocalDate startDate, LocalDate endDate);
}