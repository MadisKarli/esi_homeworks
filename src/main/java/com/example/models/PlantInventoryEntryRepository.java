package com.example.models;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PlantInventoryEntryRepository extends JpaRepository<PlantInventoryEntry, Long> {
	@Query(value = "select * from plant_inventory_entry where name like CONCAT('%',?1,'%')", nativeQuery=true)
	List<PlantInventoryEntry> findByNameContaining(String str);
	@Query(value = "select pie.id, pie.description, pie.name, pie.price from plant_inventory_item as pii join plant_inventory_entry as pie where pii.plant_info_id = pie.id and pii.equipment_condition = 'SERVICEABLE' and pie.name like CONCAT('%',?1,'%') and pie.id not in (select pr.plant_id from plant_reservation as pr where not(?2 <= pr.start_date) or ?3 > pr.end_date)", nativeQuery=true)
	List<PlantInventoryEntry> getAvailablePlantsForRental(String name, LocalDate startDate, LocalDate endDate);
}