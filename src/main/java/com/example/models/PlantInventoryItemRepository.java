package com.example.models;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PlantInventoryItemRepository extends JpaRepository<PlantInventoryItem, Long>{
		@Query(value = "select cast(case when count(*)>0 then 1 else 0 end as bit) from plant_inventory_item as pii join plant_inventory_entry as pie where pii.plant_info_id = pie.id and pii.equipment_condition = 'SERVICEABLE' and pie.id like ?1 and pie.id not in (select pr.plant_id from plant_reservation as pr where not(?2 <= pr.start_date) or ?3 > pr.end_date)", nativeQuery=true)
	boolean isItemAvailableForRental(int entryId, LocalDate startDate, LocalDate endDate);

	@Query(value = "select * from plant_inventory_item where plant_info_id not in (select id from plant_reservation where end_date > dateadd('month', -6, getdate()))", nativeQuery=true)
	List<PlantInventoryItem> hasNotBeenHired();
}