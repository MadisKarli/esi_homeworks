package com.example.models;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by gurudas on 21.02.17.
 */
public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, Long> {
}
