package com.example.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by Joonas Papoonas on 22/02/2017.
 */
@Data
@AllArgsConstructor
public class YearSum implements Serializable {
    int year;
    BigDecimal sum;
}
