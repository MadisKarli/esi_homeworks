package com.example.models;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by gurudas on 21.02.17.
 */
@Repository
public interface MaintenancePlanRepository extends JpaRepository<MaintenancePlan, Long> {
	@Query("select new com.example.models.YearCount(year_of_action, count(*)) from MaintenancePlan where year_of_action >= cast(year(getdate())-5 as integer) group by year_of_action")
	List<YearCount> getCorrectiveRepairsPerYear();
}