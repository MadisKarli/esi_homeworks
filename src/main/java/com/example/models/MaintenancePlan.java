package com.example.models;

import lombok.Data;

import javax.persistence.*;
import java.time.Year;
import java.util.List;

/**
 * Created by gurudas on 21.02.17.
 */
@Entity
@Data
public class MaintenancePlan {
    @Id @GeneratedValue
    Long id;
    int year_of_action;

    @OneToMany(cascade={CascadeType.ALL})
    List<MaintenanceTask> tasks;

    @OneToOne
    PlantInventoryItem plant;
}
