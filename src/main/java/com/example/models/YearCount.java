package com.example.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.time.Year;

/**
 * Created by Joonas Papoonas on 22/02/2017.
 */
@Data
@AllArgsConstructor
public class YearCount implements Serializable {
    int year;
    long count;
}
