package com.example.models;

/**
 * Created by gurudas on 20.02.17.
 */
public enum POStatus {
    PENDING, OPEN, REJECTED, CLOSED
}
