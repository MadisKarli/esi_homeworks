package com.example.models;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by gurudas on 21.02.17.
 */

@Repository
public interface MaintenanceTaskRepository extends JpaRepository<MaintenanceTask, Long> {
	@Query("select new com.example.models.YearSum(mp.year_of_action, (select sum(mt.price) from MaintenanceTask mt where year(mt.rentalPeriod.endDate) = mp.year_of_action)) from MaintenancePlan mp where mp.year_of_action >= cast(year(getdate())-5 as integer) group by mp.year_of_action")
	List<YearSum> getCorrectiveRepairsPricePerYear();
}
