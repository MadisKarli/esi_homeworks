package com.example.services;

import org.springframework.stereotype.Service;

/**
 * Created by lgarcia on 2/10/2017.
 */
@Service
public class HelloService {
    public String sayHello() {
        return "Hello world";
    }
}
